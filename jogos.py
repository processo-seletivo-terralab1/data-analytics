from forca import hangman_game
from adivinhacao import guessing_game

def choose_game():
    print('Escolha um jogo!')
    print('(1) Forca (2) Adivinhação')

    game = int(input('Qual jogo? '))

    if game == 1:
        # Starts the hangman game.
        hangman_game()

    elif game == 2:
        # Starts the guessing game.
        guessing_game()

if __name__ == '__main__':
    choose_game()