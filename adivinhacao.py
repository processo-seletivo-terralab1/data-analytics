from random import randrange

def chose_difficulty():
    print('Qual nível de dificuldade? ')
    print('(1) Fácil (2) Médio (3) Difícil')

    difficult = int(input('Define a dificuldade: '))

    if difficult == 1:
        remaining_chances = 20

    elif difficult == 2:
        remaining_chances = 10

    else:
        remaining_chances = 5

    return remaining_chances


def input_number():
    shot = int(input('Digite um número entre 1 e 100: '))
    print('Você digitou: ', shot)

    # Do more things with shot ...

    return shot


def guessing_game():
    print('Bem vindo ao jogo de Adivinhação!')

    remaining_chances = chose_difficulty()
    secret_number = randrange(1, 101)
    points = 1000

    for round in range(1, remaining_chances + 1):
        print('Tentativa {} de {}'.format(round, remaining_chances))

        shot = input_number()

        if shot < 1 or shot > 100:
            print('Número inválido.')
            continue

        if shot == secret_number:
            print('Parabéns! Você acertou! E fez {} pontos!'.format(points))
            break

        else:
            if shot > secret_number:
                print('Você errou. O seu chute foi maior que o número secreto.')

            elif shot < secret_number:
                print('Você errou. O seu chute foi menor do que o número secreto.')

            points_loss = abs(secret_number - shot)
            points -= points_loss

    print('Fim do jogo! Obrigado por jogar!')


if __name__ == '__main__':
    guessing_game()