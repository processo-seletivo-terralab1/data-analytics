# Data Analytics

Author: Mateus Jorge <br />

[Linkedln profile](https://www.linkedin.com/in/mateusjorge28186/) <br />
[GitHub profile](https://github.com/mavismmg) <br />
[Kaggle profile](https://www.kaggle.com/mateusjorge) <br />   

This repository contains the sprints from 1 to 5 to 'Processo Seletivo 22.1' 

![](certificado-sprint1.png)

# Sprint 1 - Códigos do jogo de adivinhação

**Primeiro código referente ao curso da Alura** <br/>

Artefato gerado: <br/>

```ruby
print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************')

numero_secreto = 9
chute = int(input('Digite o seu número: '))
print('Você digitou: ', chute)

if numero_secreto == chute:
    print('Você acertou!')
else:
    print('Você errou...')

print('*********************************')
print('Fim do jogo.')
print('*********************************')

```

**Segundo código referente ao curso da Alura** <br/>

Artefato gerado, adicionado a ```Features: dicas para o jogador``` e mudanças na legibilidade do código. Novo código:<br />

```ruby
print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************\n')

numero_secreto = 9
chute = int(input('Digite o seu número: '))
print('Você digitou: ', chute)

""" Condições do jogo de adivinhação. """
acertou     = (chute == numero_secreto)
chute_maior = (chute > numero_secreto)
chute_menor = (chute < numero_secreto)

if acertou:
    print('Você acertou!')
else:
    if chute_maior:
        print('Você errou. O seu chute foi maior que o número secreto.')
    elif chute_menor:
        print('Você errou. O seu chute foi menor do que o número secreto.')

print('\n*********************************')
print('Fim do jogo.')
print('*********************************')
```

**Terceiro código referente ao curso da Alura** <br/>

Artefato gerado, introdução do laço de repetição ```while```:

```ruby
print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************\n')

numero_secreto = 9
total_de_tentativas = 3
rodada = 1

while rodada <= total_de_tentativas:
    print('Tentativa', rodada, 'de', total_de_tentativas)

    chute = int(input('Digite o seu número: '))
    print('Você digitou: ', chute)

    """ Condições do jogo de adivinhação. """
    acertou     = (chute == numero_secreto)
    chute_maior = (chute > numero_secreto)
    chute_menor = (chute < numero_secreto)

    if acertou:
        print('Você acertou!')
    else:
        if chute_maior:
            print('Você errou. O seu chute foi maior que o número secreto.')
        elif chute_menor:
            print('Você errou. O seu chute foi menor do que o número secreto.')

    rodada += 1

print('\n*********************************')
print('Fim do jogo.')
print('*********************************')
```

**Quarto código referente ao curso da Alura** <br/>

Artefato gerado, introdução de interpolação de strings utilizando o ```.format(arg1, arg2, ...)```.<br/>

Exemplo de utilização e como utilizar o ```.format()```, suponha que exista uma função que retorne o dia e outra que retorne a semana:

```ruby
dia = dia_da_semana() # dia_da_semana() -> return int. Ex: return 20.
semana = semana()     # semana() -> return int. Ex: return 2.

""" Utilização:
    
    * {} -> recebe o valor referente ao .format() na posição indicada:
    ** Obs: caso a posição não seja indicado segue a sequência padrão
    
        - '{exibe_valor_pos1} {exibe_valor_pos2}'.format(pos1, pos2) -> return pos1 pos2
        
    ** E caso indicada:
    
        - '{1} {0}'.format(pos1, pos2) -> return pos2 pos1
"""

print('Dia {} referente a semana {}'.format(dia, semana))
```

Em seguida, código referente ao quarto artefato gerado:

```ruby
print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************\n')

numero_secreto = 9
total_de_tentativas = 3
rodada = 1

while rodada <= total_de_tentativas:
    print('Tentativa {} de {}'.format(rodada, total_de_tentativas))

    chute = int(input('Digite o seu número: '))
    print('Você digitou: ', chute)

    """ Condições do jogo de adivinhação. """
    acertou     = (chute == numero_secreto)
    chute_maior = (chute > numero_secreto)
    chute_menor = (chute < numero_secreto)

    if acertou:
        print('Você acertou!')
    else:
        if chute_maior:
            print('Você errou. O seu chute foi maior que o número secreto.')
        elif chute_menor:
            print('Você errou. O seu chute foi menor do que o número secreto.')

    rodada += 1

print('\n*********************************')
print('Fim do jogo.')
print('*********************************')
```

**Quinto código referente ao curso da Alura** <br/>

Artefato gerado, substituição do while por for:

```ruby
print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************\n')

numero_secreto = 9
total_de_tentativas = 3

for rodada in range(1, total_de_tentativas + 1):
    print('Tentativa {} de {}'.format(rodada, total_de_tentativas))

    chute = int(input('Digite o seu número: '))
    print('Você digitou: ', chute)

    """ Condições do jogo de adivinhação. """
    acertou     = (chute == numero_secreto)
    chute_maior = (chute > numero_secreto)
    chute_menor = (chute < numero_secreto)

    if acertou:
        print('Você acertou!')
    else:
        if chute_maior:
            print('Você errou. O seu chute foi maior que o número secreto.')
        elif chute_menor:
            print('Você errou. O seu chute foi menor do que o número secreto.')

print('\n*********************************')
print('Fim do jogo.')
print('*********************************')
```

**Sexto código referente ao curso da Alura** <br/>

Artefato gerado, introdução do break e do continue:

```ruby
print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************\n')

numero_secreto = 9
total_de_tentativas = 3

for rodada in range(1, total_de_tentativas + 1):
    print('Tentativa {} de {}'.format(rodada, total_de_tentativas))

    chute = int(input('Digite um número entre 1 e 100: '))
    print('Você digitou: ', chute)

    if chute < 1 or chute > 100:
        print('Você digitou um número inválido!')
        continue

    """ Condições do jogo de adivinhação. """
    acertou     = (chute == numero_secreto)
    chute_maior = (chute > numero_secreto)
    chute_menor = (chute < numero_secreto)

    if acertou:
        print('Você acertou!')
        break

    else:
        if chute_maior:
            print('Você errou. O seu chute foi maior que o número secreto.')
        elif chute_menor:
            print('Você errou. O seu chute foi menor do que o número secreto.')

print('\n*********************************')
print('Fim do jogo.')
print('*********************************')
```

**Sétimo código referente ao curso da Alura** <br/>

Artefato gerado, introdução de importação de módulos em Python e utilização da ```random``` e ```randrange```:

```ruby
import random

print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************\n')

numero_secreto = random.randrange(1, 101)
total_de_tentativas = 3

for rodada in range(1, total_de_tentativas + 1):
    print('Tentativa {} de {}'.format(rodada, total_de_tentativas))

    chute = int(input('Digite um número entre 1 e 100: '))
    print('Você digitou: ', chute)

    if chute < 1 or chute > 100:
        print('Você digitou um número inválido!')
        continue

    """ Condições do jogo de adivinhação. """
    acertou     = (chute == numero_secreto)
    chute_maior = (chute > numero_secreto)
    chute_menor = (chute < numero_secreto)

    if acertou:
        print('Você acertou!')
        break

    else:
        if chute_maior:
            print('Você errou. O seu chute foi maior que o número secreto.')
        elif chute_menor:
            print('Você errou. O seu chute foi menor do que o número secreto.')

print('\n*********************************')
print('Fim do jogo.')
print('*********************************')
```

**Oitavo código referente ao curso da Alura** <br/>

Artefato gerado, ```Feature: escolha de dificuldade e pontuação```:

```ruby
import random

print('********************************')
print('Bem vindo ao jogo de Adivinhação')
print('********************************\n')

numero_secreto = random.randrange(1, 101)
total_de_tentativas = 0
pontos = 1000

print('Qual nível de dificuldade?')
print('(1) Fácil (2) Médio (3) Difícil')

nivel = int(input('Define o nível: '))

if nivel == 1:
    total_de_tentativas = 20
elif nivel == 2:
    total_de_tentativas = 10
else:
    total_de_tentativas = 5

for rodada in range(1, total_de_tentativas + 1):
    print('Tentativa {} de {}'.format(rodada, total_de_tentativas))

    chute = int(input('Digite um número entre 1 e 100: '))
    print('Você digitou: ', chute)

    if chute < 1 or chute > 100:
        print('Você digitou um número inválido!')
        continue

    """ Condições do jogo de adivinhação. """
    acertou     = (chute == numero_secreto)
    chute_maior = (chute > numero_secreto)
    chute_menor = (chute < numero_secreto)

    if acertou:
        print('Você acertou e fez {} pontos!'.format(pontos))
        break

    else:
        if chute_maior:
            print('Você errou. O seu chute foi maior que o número secreto.')
        elif chute_menor:
            print('Você errou. O seu chute foi menor do que o número secreto.')

        pontos_perdidos = abs(numero_secreto - chute)
        pontos = pontos - pontos_perdidos

print('\n*********************************')
print('Fim do jogo.')
print('*********************************')
```

**Nono código referente ao curso da Alura** <br/>

Artefato gerados, introdução a funções em Python e de controle de escopo. Ademais, foi introduzido um novo jogo chamado ```forca.py``` e o arquivo ```jogos.py``` que possibilita ao jogador escolher entre um dos jogos.

```adivinhacao.py```

```ruby
import random

def jogar():
    print('********************************')
    print('Bem vindo ao jogo de Adivinhação')
    print('********************************\n')

    numero_secreto = random.randrange(1, 101)
    total_de_tentativas = 0
    pontos = 1000

    print('Qual nível de dificuldade?')
    print('(1) Fácil (2) Médio (3) Difícil')

    nivel = int(input('Define o nível: '))

    if nivel == 1:
        total_de_tentativas = 20
    elif nivel == 2:
        total_de_tentativas = 10
    else:
        total_de_tentativas = 5

    for rodada in range(1, total_de_tentativas + 1):
        print('Tentativa {} de {}'.format(rodada, total_de_tentativas))

        chute = int(input('Digite um número entre 1 e 100: '))
        print('Você digitou: ', chute)

        if chute < 1 or chute > 100:
            print('Você digitou um número inválido!')
            continue

        """ Condições do jogo de adivinhação. """
        acertou     = (chute == numero_secreto)
        chute_maior = (chute > numero_secreto)
        chute_menor = (chute < numero_secreto)

        if acertou:
            print('Você acertou e fez {} pontos!'.format(pontos))
            break

        else:
            if chute_maior:
                print('Você errou. O seu chute foi maior que o número secreto.')
            elif chute_menor:
                print('Você errou. O seu chute foi menor do que o número secreto.')

            pontos_perdidos = abs(numero_secreto - chute)
            pontos = pontos - pontos_perdidos

    print('\n*********************************')
    print('Fim do jogo.')
    print('*********************************')

if __name__ == '__main__':
    jogar()
```

```forca.py```

```ruby
def jogar():
    print('********************************')
    print('Bem vindo ao jogo da Forca')
    print('********************************\n')

    print('Fim de Jogo')

if __name__ == '__main__':
    jogar()
```

```jogos.py```

```ruby
import forca
import adivinhacao

def escolhe_jogo():
    print('********************************')
    print('******* Escolha um jogo! *******')
    print('********************************\n')

    print('(1) Forca (2) Adivinhação')

    jogo = int(input('Qual jogo? '))

    if jogo == 1:
        print('Jogando forca.')
        forca.jogar()
    elif jogo == 2:
        print('Jogando advinhação.')
        adivinhacao.jogar()

if __name__ == '__main__':
    escolhe_jogo()
```

**Extra: código final** <br/>

Código final, para melhorar a organização do código e transformá-lo para padrão internacional de leitura, foi introduzido ideais de simplificação propostas de clean code e traduzido para inglês.

```jogos.py```

```ruby
from forca import hangman_game
from adivinhacao import guessing_game

def choose_game():
    print('Escolha um jogo!')
    print('(1) Forca (2) Adivinhação')

    game = int(input('Qual jogo? '))

    if game == 1:
        # Starts the hangman game.
        hangman_game()

    elif game == 2:
        # Starts the guessing game.
        guessing_game()

if __name__ == '__main__':
    choose_game()
```

```adivinhacao.py```

```ruby
from random import randrange

def chose_difficulty():
    print('Qual nível de dificuldade? ')
    print('(1) Fácil (2) Médio (3) Difícil')

    difficult = int(input('Define a dificuldade: '))

    if difficult == 1:
        remaining_chances = 20

    elif difficult == 2:
        remaining_chances = 10

    else:
        remaining_chances = 5

    return remaining_chances


def input_number():
    shot = int(input('Digite um número entre 1 e 100: '))
    print('Você digitou: ', shot)

    # Do more things with shot ...

    return shot


def guessing_game():
    print('Bem vindo ao jogo de Adivinhação!')

    remaining_chances = chose_difficulty()
    secret_number = randrange(1, 101)
    points = 1000

    for round in range(1, remaining_chances + 1):
        print('Tentativa {} de {}'.format(round, remaining_chances))

        shot = input_number()

        if shot < 1 or shot > 100:
            print('Número inválido.')
            continue

        if shot == secret_number:
            print('Parabéns! Você acertou! E fez {} pontos!'.format(points))
            break

        else:
            if shot > secret_number:
                print('Você errou. O seu chute foi maior que o número secreto.')

            elif shot < secret_number:
                print('Você errou. O seu chute foi menor do que o número secreto.')

            points_loss = abs(secret_number - shot)
            points -= points_loss

    print('Fim do jogo! Obrigado por jogar!')


if __name__ == '__main__':
    guessing_game()
```

```forca.py```

```ruby
def hangman_game():
    print('Not yet implemented!')

    # Game logic here ...

    print('Fim de Jogo')


if __name__ == '__main__':
    hangman_game()
```